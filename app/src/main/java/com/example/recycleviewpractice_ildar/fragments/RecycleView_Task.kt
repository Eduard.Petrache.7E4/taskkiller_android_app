package com.example.recycleviewpractice_ildar.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener_Task
import com.example.recycleviewpractice_ildar.R
import com.example.recycleviewpractice_ildar.adapters.ListaAdapter
import com.example.recycleviewpractice_ildar.adapters.TaskAdapter
import com.example.recycleviewpractice_ildar.databinding.FragmentRecycleViewBinding
import com.example.recycleviewpractice_ildar.databinding.FragmentRecycleViewTaskBinding
import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.models.Task
import com.example.recycleviewpractice_ildar.viewModel.ViewModel

class RecycleView_Task : Fragment(), OnClickListener_Task {

    private lateinit var taskAdapter: TaskAdapter;
    private lateinit var binding: FragmentRecycleViewTaskBinding
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    val viewModel: ViewModel by activityViewModels()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding= FragmentRecycleViewTaskBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var list=arguments?.get("lista") as Lista //recibimos los datos del fragment anterior y casteamos de any a student

        val newTask=binding.addButton
        taskAdapter = TaskAdapter(viewModel.getListTasks(list),this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = taskAdapter
        }



        viewModel.taskModel.observe(viewLifecycleOwner, Observer {taskasActuales->

            taskAdapter= TaskAdapter(viewModel.getListTasks(list),this)
        })


        newTask.setOnClickListener{
            println("new task "+list.tasks.size)
            viewModel.createNewTask(list," ")
        }

    }


    override fun onClick(task: Task) {

        println("implementa para selecionar taskas ")
    }


}