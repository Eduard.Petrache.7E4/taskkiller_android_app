package com.example.recycleviewpractice_ildar.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.recycleviewpractice_ildar.databinding.FragmentCreateBinding
import com.example.recycleviewpractice_ildar.viewModel.ViewModel


class CreateFragment : Fragment() {

    lateinit var binding: FragmentCreateBinding
   val viewModel: ViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding= FragmentCreateBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name=binding.nameContent

        val returnButton=binding.createStudentButton
        returnButton.setOnClickListener{
            val action =CreateFragmentDirections.actionCreateFragmentToRecycleViewFragment2()
            addNewList(name.text.toString().trim());
            findNavController().navigate(action)
        }

    }

    private fun addNewList(name:String) {
        if(name.length>0 && !name.equals("Name")){//si hay algo escrito en el nombre

            viewModel.createNewList(name)//creamos nuevo student en view model
        }
    }


}