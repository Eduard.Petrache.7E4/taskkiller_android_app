package com.example.recycleviewpractice_ildar.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener
import com.example.recycleviewpractice_ildar.R
import com.example.recycleviewpractice_ildar.databinding.ItemListBinding
import com.example.recycleviewpractice_ildar.models.Lista

class ListaAdapter (private val listas: List<Lista>, private val listener: OnClickListener)
:RecyclerView.Adapter<ListaAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemListBinding.bind(view)

        fun setListener(lista: Lista) {
            binding.root.setOnClickListener {
                listener.onClick(lista)
            }
        }

    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lista = listas[position]
        with(holder){
            setListener(lista)
            binding.name.text = lista.name
            // Glide.with(context)
            //     .load(Lista.url)
            //     .diskCacheStrategy(DiskCacheStrategy.ALL)
            //     .centerCrop()
            //     .circleCrop()
            //     .into(binding.photo)

        }
    }

    override fun getItemCount(): Int {
        return listas.size
    }
}