package com.example.recycleviewpractice_ildar.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener
import com.example.recycleviewpractice_ildar.OnClickListener_Task
import com.example.recycleviewpractice_ildar.R
import com.example.recycleviewpractice_ildar.databinding.ItemTaskBinding
import com.example.recycleviewpractice_ildar.models.Task

class TaskAdapter(private val tasks: List<Task>, private val listener: OnClickListener_Task)
    : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemTaskBinding.bind(view)

        fun setListener(task: Task) {
            binding.root.setOnClickListener {
                listener.onClick(task)
            }
        }

    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_task, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]
        with(holder){
            setListener(task)
            binding.contentEditText.setText( task.content)
            // Glide.with(context)
            //     .load(Lista.url)
            //     .diskCacheStrategy(DiskCacheStrategy.ALL)
            //     .centerCrop()
            //     .circleCrop()
            //     .into(binding.photo)

        }
    }

    override fun getItemCount(): Int {
        return tasks.size
    }
}