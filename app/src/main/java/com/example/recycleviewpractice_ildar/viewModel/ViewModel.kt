package com.example.recycleviewpractice_ildar.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.recycleviewpractice_ildar.models.Lista
import com.example.recycleviewpractice_ildar.models.Task

class ViewModel: ViewModel() {


    val listaModel= MutableLiveData<MutableList<Lista>>()
    val taskModel= MutableLiveData<MutableList<Task>>()

    val listas = mutableListOf<Lista>() //todos los estudiantes
    val taskas = mutableListOf<Task>() //todos los estudiantes



    fun createNewList(name:String){
         var emptyTasks= mutableListOf<Task>()
         var list=Lista(name,1 , emptyTasks)
        listas.add(list)
        listaModel.postValue(listas)
      }

    fun createNewTask(list:Lista, content:String){ //creamos nueva tarea en una lista

        var newTask=Task(content,false,1)

        taskas.add(newTask)

        listas.forEach{lista ->
            if (lista.name.equals(list.name)){
                lista.tasks.add(newTask)
            }
            listaModel.postValue(listas)
            taskModel.postValue(taskas)

        }
    }

    fun getListTasks(list: Lista):MutableList<Task>{
        listas.forEach{lista ->
            if (lista.name.equals(list.name)){
                return lista.tasks
            }
    }
        return list.tasks
    }

    //VjewFragments
    //mutableListOf<Student>()


    //StudentAdapter
    //Name, Asist, Justfied


}