package com.example.recycleviewpractice_ildar

import com.example.recycleviewpractice_ildar.models.Lista

interface OnClickListener {

    fun onClick(lista: Lista)

}