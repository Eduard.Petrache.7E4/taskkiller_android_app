package com.example.recycleviewpractice_ildar.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Lista( var name: String , var id:Int, var tasks: MutableList<Task>): Parcelable
//poner un mutableList<Task> en el id