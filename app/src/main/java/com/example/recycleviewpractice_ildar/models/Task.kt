package com.example.recycleviewpractice_ildar.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Task( var content: String, var done:Boolean, var list_id:Int): Parcelable
