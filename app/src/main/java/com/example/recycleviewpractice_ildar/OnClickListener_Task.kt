package com.example.recycleviewpractice_ildar

import com.example.recycleviewpractice_ildar.models.Task

interface OnClickListener_Task {

    fun onClick(task: Task)

}
